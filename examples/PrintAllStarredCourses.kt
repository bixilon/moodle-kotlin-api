/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

import de.bixilon.moodle.api.async.AsyncMoodleAPI

object PrintAllStarredCourses {
    private lateinit var moodleAPI: AsyncMoodleAPI

    @JvmStatic
    fun main(string: Array<String>) {
        // we must specify a url
        moodleAPI = AsyncMoodleAPI("https://super.secret.moodle.url")
        // also possible with access token like this
        // moodleAPI = AsyncMoodleAPI("https://super.secret.moodle.url", "secret access token")

        // If we don't specify a access token, we need to login. Only possible if the server supports it.
        moodleAPI.login("username", "super_secret_password")

        // this is important, it fetches the user id, etc. You must run this before anything else
        moodleAPI.initAPI()

        // now we want a list of courses.
        // because this runs async, we should specify a callback, we could also ::waitForFulfill, but probably you don't want this
        moodleAPI.requestAllStarredCourses {
            // loop over all courses
            for (course in it) {
                println(course.fullName)
            }

            // because we fetched all data, we want to stop everything (e.g clean up and stop executor service). After that the API won't function anymore
            moodleAPI.shutdown()
        }
    }
}
