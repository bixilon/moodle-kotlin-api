# Moodle Kotlin (Java) API

[Moodle](https://moodle.org/?lang=de) is an open source learning platform. This library is a wrapper for the [WebServices API](https://docs.moodle.org/dev/Web_service_API_functions).

## Well known moodle instances

- [**Me**dien, **Bi**ldung, **S**ervice](https://mebis.bayern.de/)

## Examples

Take a look at [/examples](/examples)

## Requirements

- Java 11+ (latest version recommended)
- Moodle requirements:
    - Enabled Webservices (Administration/Additional services)
    - Enabled Webservices for mobile API (Administration/Mobile App/Mobile Settings)

## Usages

- Own client implementations
- Notification service (with automated polling of data, etc)
- Automated administration tasks (consider using moodle plugins)
- Botting

## How to use it in your program

1. Add JitPack to your Repository
2. Add this repository to your dependencies.
3. Optional: Set up a local moodle instance to test and look at the API documentation
4. Look at the examples and get ready to code

```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

```xml
<dependencies>
    <dependency>
        <groupId>de.bixilon.gitlab.bixilon</groupId>
        <artifactId>moodle-kotlin-api</artifactId>
        <version>Tag</version>
    </dependency>
</dependencies>
```

Replace `Tag` with the current release or commit

## Additional words

This is very wip, things might change until the first release. Many things need to be implemented
