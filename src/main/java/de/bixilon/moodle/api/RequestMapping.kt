/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api

import com.google.common.collect.HashBiMap
import de.bixilon.moodle.api.requests.Request
import de.bixilon.moodle.api.requests.block.starredcourses.BlockStarredCoursesGetStarredCoursesRequest
import de.bixilon.moodle.api.requests.core.message.*
import de.bixilon.moodle.api.requests.core.webservice.CoreWebserviceGetSiteInfo
import kotlin.reflect.KClass

object RequestMapping {
    private val REQUEST_MAPPING = HashBiMap.create(
        mapOf(
            "block_starredcourses_get_starred_courses" to BlockStarredCoursesGetStarredCoursesRequest::class,
            "core_message_get_messages" to CoreMessageGetMessagesRequest::class,
            "core_message_get_conversation_messages" to CoreMessageGetConversationMessagesRequest::class,
            "core_webservice_get_site_info" to CoreWebserviceGetSiteInfo::class,
            "core_message_send_instant_messages" to CoreMessageSendInstantMessagesRequest::class,
            "core_message_get_conversations" to CoreMessageGetConversationsRequest::class,
            "core_message_send_messages_to_conversation" to CoreMessageSendMessagesToConversationRequest::class,
        )
    )!!

    fun getPacketByFunction(function: String): KClass<out Request>? {
        return REQUEST_MAPPING[function]
    }

    fun getFunctionByPacket(clazz: KClass<out Request>): String? {
        return REQUEST_MAPPING.inverse()[clazz]
    }

    fun registerPacket(function: String, clazz: KClass<out Request>) {
        REQUEST_MAPPING[function] = clazz
    }
}
