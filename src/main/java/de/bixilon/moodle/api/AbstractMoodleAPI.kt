/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api

import de.bixilon.moodle.api.exceptions.AuthenticationException
import de.bixilon.moodle.api.exceptions.InvalidDataException
import de.bixilon.moodle.api.http.HTTP
import de.bixilon.moodle.api.objects.SiteInfo
import de.bixilon.moodle.api.requests.Request
import de.bixilon.moodle.api.serializer.JSONSerializer

abstract class AbstractMoodleAPI(url: String) {
    private val url: String = url.replace("\\/+", "/").removeSuffix("/")
    protected val endpointURL: String = url + de.bixilon.moodle.api.MoodleConstants.DEFAULT_ENDPOINT
    private lateinit var accessToken: String
    protected lateinit var siteInfo: SiteInfo

    constructor(url: String, accessToken: String) : this(url) {
        this.accessToken = accessToken
    }

    @Throws(AuthenticationException::class)
    fun login(username: String, password: String): String {
        val response = HTTP.getJson(url + de.bixilon.moodle.api.MoodleConstants.LOGIN_ENDPOINT, mapOf("username" to username, "password" to password, "service" to "moodle_mobile_app")) as Map<String, Any>
        if (response.containsKey("errorcode")) {
            throw AuthenticationException(response["error"]!! as String)
        }
        this.accessToken = response["token"]!! as String
        return accessToken
    }

    protected fun prepareRequest(request: Request): Map<String, Any> {
        val data = mutableMapOf<String, Any>()
        request.write(data)
        if (data.containsKey(de.bixilon.moodle.api.MoodleConstants.TOKEN_FIELD_NAME)) {
            throw InvalidDataException("${de.bixilon.moodle.api.MoodleConstants.TOKEN_FIELD_NAME} is a registered keyword!")
        }
        if (data.containsKey(de.bixilon.moodle.api.MoodleConstants.FUNCTION_FIELD_NAME)) {
            throw InvalidDataException("${de.bixilon.moodle.api.MoodleConstants.FUNCTION_FIELD_NAME} is a registered keyword!")
        }
        if (data.containsKey(de.bixilon.moodle.api.MoodleConstants.REST_FORMAT_FIELD_NAME)) {
            throw InvalidDataException("${de.bixilon.moodle.api.MoodleConstants.REST_FORMAT_FIELD_NAME} is a registered keyword!")
        }
        data[de.bixilon.moodle.api.MoodleConstants.TOKEN_FIELD_NAME] = accessToken
        data[de.bixilon.moodle.api.MoodleConstants.FUNCTION_FIELD_NAME] = de.bixilon.moodle.api.RequestMapping.getFunctionByPacket(request::class)!!
        data[de.bixilon.moodle.api.MoodleConstants.REST_FORMAT_FIELD_NAME] = de.bixilon.moodle.api.MoodleConstants.REST_FORMAT_FIELD_VALUE
        return de.bixilon.moodle.api.AbstractMoodleAPI.Companion.formatDataToMoodle(data.toMap())
    }

    abstract fun initAPI()

    abstract fun shutdown()

    companion object {
        private fun formatMapToMoodle(keyPrefix: String, output: MutableMap<String, Any>, data: Map<String, Any>) {
            for ((key, value) in data) {
                val currentPrefix = if (keyPrefix.isBlank()) {
                    key
                } else {
                    "$keyPrefix[$key]"
                }
                de.bixilon.moodle.api.AbstractMoodleAPI.Companion.formatDataToMoodle(currentPrefix, output, value)
            }

        }

        private fun formatDataToMoodle(keyPrefix: String, output: MutableMap<String, Any>, data: Any) {
            when (data) {
                is Map<*, *> -> {
                    de.bixilon.moodle.api.AbstractMoodleAPI.Companion.formatMapToMoodle(keyPrefix, output, data as Map<String, Any>)
                }
                is List<*> -> {
                    de.bixilon.moodle.api.AbstractMoodleAPI.Companion.formatListToMoodle(keyPrefix, output, data as List<Any>)
                }
                else -> {
                    if (data::class.isData) {
                        de.bixilon.moodle.api.AbstractMoodleAPI.Companion.formatMapToMoodle(keyPrefix, output, JSONSerializer.objectToMap(data))
                    } else {
                        output[keyPrefix] = data
                    }
                }
            }
        }

        private fun formatListToMoodle(keyPrefix: String, output: MutableMap<String, Any>, data: List<Any>) {
            for ((i, entry) in data.withIndex()) {
                de.bixilon.moodle.api.AbstractMoodleAPI.Companion.formatDataToMoodle("$keyPrefix[${i}]", output, entry)
            }
        }

        private fun formatDataToMoodle(data: Map<String, Any>): Map<String, Any> {
            val output: MutableMap<String, Any> = mutableMapOf()
            de.bixilon.moodle.api.AbstractMoodleAPI.Companion.formatMapToMoodle("", output, data)
            return output
        }
    }

}
