/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api

object MoodleConstants {
    const val DEFAULT_ENDPOINT = "/webservice/rest/server.php"
    const val LOGIN_ENDPOINT = "/login/token.php"

    const val TOKEN_FIELD_NAME = "wstoken"
    const val FUNCTION_FIELD_NAME = "wsfunction"
    const val REST_FORMAT_FIELD_NAME = "moodlewsrestformat"
    const val REST_FORMAT_FIELD_VALUE = "json"
}
