/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.async

import de.bixilon.moodle.api.http.HTTP
import de.bixilon.moodle.api.objects.Conversation
import de.bixilon.moodle.api.objects.Course
import de.bixilon.moodle.api.objects.Message
import de.bixilon.moodle.api.objects.SiteInfo
import de.bixilon.moodle.api.requests.Request
import de.bixilon.moodle.api.requests.block.starredcourses.BlockStarredCoursesGetStarredCoursesRequest
import de.bixilon.moodle.api.requests.core.message.CoreMessageGetConversationsRequest
import de.bixilon.moodle.api.requests.core.message.CoreMessageGetMessagesRequest
import de.bixilon.moodle.api.requests.core.webservice.CoreWebserviceGetSiteInfo
import java.util.concurrent.Executors

class AsyncMoodleAPI : de.bixilon.moodle.api.AbstractMoodleAPI {
    private val executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())!!

    constructor(url: String, accessToken: String) : super(url, accessToken)

    constructor(url: String) : super(url)

    fun <T> request(request: Request, onFullFill: ((T) -> Unit)? = null): Promise<T> {
        val promise = Promise<T>(onFullFill)
        executor.execute {
            promise.fulfill(request.readResponse(this, HTTP.getJson(endpointURL, prepareRequest(request))) as T)
        }
        return promise
    }

    fun requestAllStarredCourses(onFullFill: ((List<Course>) -> Unit)? = null): Promise<List<Course>> {
        return request(BlockStarredCoursesGetStarredCoursesRequest(), onFullFill)
    }

    fun getMessages(userIdTo: Int, onFullFill: ((List<Message>) -> Unit)? = null): Promise<List<Message>> {
        return request(CoreMessageGetMessagesRequest(userIdTo), onFullFill)
    }

    fun getAllMessages(onFullFill: ((List<Message>) -> Unit)? = null): Promise<List<Message>> {
        return request(CoreMessageGetMessagesRequest(), onFullFill)
    }

    fun getMessages(onFullFill: ((List<Message>) -> Unit)? = null): Promise<List<Message>> {
        return request(CoreMessageGetMessagesRequest(siteInfo.userId), onFullFill)
    }

    fun getConversations(onFullFill: ((List<Conversation>) -> Unit)? = null): Promise<List<Conversation>> {
        return request(CoreMessageGetConversationsRequest(siteInfo.userId), onFullFill)
    }

    override fun initAPI() {
        this.siteInfo = request<SiteInfo>(CoreWebserviceGetSiteInfo()).waitForFulfill()
    }

    override fun shutdown() {
        executor.shutdown()
    }
}
