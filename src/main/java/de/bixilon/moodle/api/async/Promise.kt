/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.async

class Promise<T>(private val onFullFill: ((T) -> Unit)? = null) {
    private val lock = Object()
    private var value: T? = null
    fun getValue(): T {
        return value!!
    }

    val isFulfilled: Boolean get() = value != null

    fun fulfill(value: T) {
        this.value = value
        synchronized(lock) { lock.notifyAll() }
        onFullFill?.let { it(value) }
    }

    @Throws(InterruptedException::class)
    fun waitForFulfillI(): T {
        synchronized(lock) {
            while (!isFulfilled) {
                lock.wait()
            }
        }
        return value!!
    }

    fun waitForFulfill(): T {
        synchronized(lock) {
            while (!isFulfilled) {
                try {
                    lock.wait()
                } catch (ignored: InterruptedException) {
                }
            }
        }
        return value!!
    }
}
