/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.objects

import com.squareup.moshi.Json

data class SiteInfo(
    @Json(name = "sitename") val siteName: String,
    val username: String,
    @Json(name = "firstname") val firstName: String,
    @Json(name = "lastname") val lastName: String,
    @Json(name = "fullname") val fullName: String,
    val lang: String,
    @Json(name = "userid") val userId: Int,
    @Json(name = "siteurl") val siteURL: String,
    @Json(name = "userpictureurl") val userPictureURL: String,
    @Json(name = "downloadfiles") val downloadFiles: Boolean?,
    @Json(name = "uploadfiles") val uploadFiles: Boolean?,
    val release: String?,
    val version: String?,
    @Json(name = "mobilecssurl") val mobileCSSURL: String?,
    @Json(name = "usercanmanageownfiles") val userCanMangeOwnFiles: Boolean?,
    @Json(name = "userquota") val userQuota: Int?,
    @Json(name = "usermaxuploadfilesize") val userMaxUploadFileSize: Int?,
    @Json(name = "userhomepage") val userHomePage: Int?,
    @Json(name = "userprivateaccesskey") val userPrivateAccessKey: String?,
    @Json(name = "siteid") val siteId: Int?,
    @Json(name = "sitecalendartype") val siteCalenderType: String?,
    @Json(name = "usercalendartype") val userCalenderType: String?,
    @Json(name = "userissiteadmin") val userIsSiteAdmin: Boolean?,
    @Json(name = "theme") val theme: String?,
)
