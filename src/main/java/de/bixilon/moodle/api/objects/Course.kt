/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.objects

import com.squareup.moshi.Json
import de.bixilon.moodle.api.async.AsyncMoodleAPI
import de.bixilon.moodle.api.enums.MoodleMessageFormat

data class Course(
    @Transient var moodleAPI: AsyncMoodleAPI? = null,
    val id: Int,
    @Json(name = "fullname") val fullName: String,
    @Json(name = "shortname") val shortName: String,
    @Json(name = "idnumber") val isNumber: String,
    val summary: String,
    @Json(name = "summaryformat") val summaryFormat: MoodleMessageFormat,
    @Json(name = "startdate") val startDate: Long,
    @Json(name = "enddate") val endDate: Long,
    val visible: Boolean,
    @Json(name = "fullnamedisplay") val fullNameDisplay: String,
    @Json(name = "courseimage") val courseImage: String,
    @Json(name = "viewurl") val viewURL: String,
    val progress: Float?,
    @Json(name = "hasprogress") val hasProgress: Boolean,
    @Json(name = "isfavourite") val isFavourite: Boolean,
    val hidden: Boolean,
    @Json(name = "timeaccess") val timeAccess: Long?,
    @Json(name = "showshortname") val showShortName: Boolean,
    @Json(name = "coursecategory") val courseCategory: String,
)
