/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.objects

import com.squareup.moshi.Json

data class ConversationMember(
    val id: Int,
    val fullname: String,
    @Json(name = "profileurl") val profileURL: String,
    @Json(name = "profileimageurl") val profileImageURL: String,
    @Json(name = "profileimageurlsmall") val profileImageURLSmall: String,
    @Json(name = "isonline") val isOnline: Boolean?, // ToDo: expected non null
    @Json(name = "showonlinestatus") val showOnlineStatus: Boolean,
    @Json(name = "isblocked") val isBlocked: Boolean,
    @Json(name = "iscontact") val isContact: Boolean,
    @Json(name = "isdeleted") val isDeleted: Boolean,
    @Json(name = "canmessageevenifblocked") val canMessageEvenIfBlocked: Boolean?, // ToDo: expected non null
    @Json(name = "canmessage") val canMessage: Boolean?, // ToDo: expected non null
    @Json(name = "requirescontact") val requiresContact: Boolean?, // ToDo: expected non null
    @Json(name = "contactrequests") val contactRequests: List<ContactRequest>?,
    val conversations: List<MemberConversation>?,
)
