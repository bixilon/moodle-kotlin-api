/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.objects

import com.squareup.moshi.Json
import de.bixilon.moodle.api.async.AsyncMoodleAPI
import de.bixilon.moodle.api.enums.MoodleMessageFormat

data class Message(
    @Transient var moodleAPI: AsyncMoodleAPI? = null,
    @Json(name = "id") val siteName: Int,
    @Json(name = "useridfrom") val userIdFrom: Int,
    val subject: String?,
    val text: String,
    @Json(name = "fullmessage") val fullMessage: String,
    @Json(name = "fullmessageformat") val fullMessageFormat: MoodleMessageFormat,
    @Json(name = "fullmessagehtml") val fullMessageHTML: String,
    @Json(name = "smallmessage") val smallMessage: String,
    @Json(name = "notification") val notification: Int,
    @Json(name = "contexturl") val contextURL: String,
    @Json(name = "contexturlname") val contextURLName: String,
    @Json(name = "timecreated") val timeCreated: Long,
    @Json(name = "timeread") val timeRead: Long,
    @Json(name = "usertofullname") val userToFullName: String,
    @Json(name = "userfromfullname") val userFromFullName: String,
    val component: String?,
    @Json(name = "eventtype") val eventType: String?,
    @Json(name = "customdata") val customData: String?,
    val warnings: List<Warning>?,
)
