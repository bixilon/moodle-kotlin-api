/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.objects

import com.squareup.moshi.Json
import de.bixilon.moodle.api.async.AsyncMoodleAPI
import de.bixilon.moodle.api.async.Promise
import de.bixilon.moodle.api.enums.MoodleMessageType
import de.bixilon.moodle.api.requests.core.message.CoreMessageSendMessagesToConversationRequest

data class Conversation(
    @Transient var moodleAPI: AsyncMoodleAPI? = null,
    val id: Int,
    @Json(name = "name") val name: String?,
    @Json(name = "subname") val subName: String?,
    @Json(name = "imageurl") val imageURL: String?,
    @Json(name = "type") val type: MoodleMessageType,
    @Json(name = "membercount") val memberCount: Int,
    @Json(name = "ismuted") val isMuted: Boolean,
    @Json(name = "isfavourite") val isFavourite: Boolean,
    @Json(name = "isread") val isRead: Boolean,
    @Json(name = "unreadcount") val unreadMessagesCount: Int?,
    val members: List<ConversationMember>,
    val messages: List<ConversationMessage>,
    @Json(name = "candeletemessagesforallusers") val canDeleteMessagesForAllUsers: Boolean?,
) {
    fun sendMessages(vararg messages: ConversationMessageToSend): Promise<MessageToSendResponse> {
        return sendMessages(messages.toList())
    }

    fun sendMessages(messages: List<ConversationMessageToSend>): Promise<MessageToSendResponse> {
        return moodleAPI!!.request(CoreMessageSendMessagesToConversationRequest(id, messages.toList()))
    }

    fun sendMessages(vararg messages: String): Promise<MessageToSendResponse> {
        val messagesToSend = mutableListOf<ConversationMessageToSend>()
        for (message in messages) {
            messagesToSend.add(ConversationMessageToSend(message))
        }

        return sendMessages(messagesToSend)
    }
}
