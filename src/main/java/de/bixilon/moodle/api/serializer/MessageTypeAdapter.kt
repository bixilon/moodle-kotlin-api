/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.serializer

import com.squareup.moshi.*
import de.bixilon.moodle.api.enums.MoodleMessageType


class MessageTypeAdapter : JsonAdapter<MoodleMessageType>() {

    @ToJson
    override fun toJson(writer: JsonWriter, value: MoodleMessageType?) {
        if (value == null) {
            writer.nullValue()
            return
        }
        writer.value(value.ordinal)
    }

    @FromJson
    override fun fromJson(reader: JsonReader): MoodleMessageType? {
        return when (reader.peek()) {
            JsonReader.Token.NULL -> reader.nextNull()
            JsonReader.Token.NUMBER -> {
                MoodleMessageType.VALUES[reader.nextInt()]
            }
            JsonReader.Token.STRING -> MoodleMessageType.valueOf(reader.nextString())
            else -> throw IllegalArgumentException("Not a message type!")
        }
    }
}
