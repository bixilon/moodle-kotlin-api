/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.http

import de.bixilon.moodle.api.exceptions.InternalServerErrorException
import de.bixilon.moodle.api.exceptions.MoodleAPIErrorException
import de.bixilon.moodle.api.exceptions.UnknownHTTPResponseException
import de.bixilon.moodle.api.serializer.JSONSerializer
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.net.URI
import java.net.URLEncoder
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse


object HTTP {

    @Throws(IOException::class, InterruptedException::class)
    fun get(url: String): HttpResponse<String> {
        return HttpClient.newHttpClient().send(HttpRequest.newBuilder().uri(URI.create(url)).build(), HttpResponse.BodyHandlers.ofString())
    }

    private fun urlEncodeUTF8(s: String): String {
        return try {
            URLEncoder.encode(s, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            throw UnsupportedOperationException(e)
        }
    }

    private fun urlEncodeUTF8(data: Map<String, Any>): String {
        val builder = StringBuilder()
        for ((key, value) in data) {
            if (builder.isNotEmpty()) {
                builder.append("&")
            }
            builder.append(String.format("%s=%s", urlEncodeUTF8(key), urlEncodeUTF8(value.toString())))
        }
        return builder.toString()
    }

    @Throws(IOException::class, InterruptedException::class)
    fun get(url: String, data: Map<String, Any>): HttpResponse<String> {
        return get(url + "?" + urlEncodeUTF8(data))
    }

    @Throws(IOException::class, InterruptedException::class)
    fun getJson(url: String, data: Map<String, Any>): Any {
        val response = get(url, data)
        if (response.statusCode() == 500) {
            throw InternalServerErrorException()
        }
        if (response.statusCode() != 200) {
            throw UnknownHTTPResponseException()
        }
        val rawJson = response.body()!!

        if (rawJson.startsWith('[')) {
            return JSONSerializer.JSON_LIST_ADAPTER.fromJson(rawJson)!!
        }
        val json = JSONSerializer.JSON_MAP_ADAPTER.fromJson(rawJson)!!
        if (json.containsKey("errorcode")) {
            throw MoodleAPIErrorException(json["message"]!! as String)
        }
        return json
    }
}
