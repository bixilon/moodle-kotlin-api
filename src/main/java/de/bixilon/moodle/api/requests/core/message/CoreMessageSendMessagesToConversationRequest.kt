/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.requests.core.message

import de.bixilon.moodle.api.async.AsyncMoodleAPI
import de.bixilon.moodle.api.objects.ConversationMessage
import de.bixilon.moodle.api.objects.ConversationMessageToSend
import de.bixilon.moodle.api.requests.Request
import de.bixilon.moodle.api.serializer.JSONSerializer

class CoreMessageSendMessagesToConversationRequest(
    private val conversationId: Int,
    private val messages: List<ConversationMessageToSend>
) : Request {

    override fun write(data: MutableMap<String, Any>) {
        data["conversationid"] = conversationId
        data["messages"] = messages
    }

    override fun readResponse(moodleAPI: AsyncMoodleAPI, data: Any): List<ConversationMessage> {
        val messages = mutableListOf<ConversationMessage>()
        for (entry in data as List<Map<*, *>>) {
            messages.add(JSONSerializer.CONVERSATION_MESSAGE_TO_SEND_RESPONSE_ADAPTER.fromJsonValue(entry)!!)
        }
        return messages.toList()
    }
}
