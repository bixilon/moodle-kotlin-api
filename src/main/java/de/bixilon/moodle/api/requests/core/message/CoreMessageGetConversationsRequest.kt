/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.requests.core.message

import de.bixilon.moodle.api.async.AsyncMoodleAPI
import de.bixilon.moodle.api.objects.Conversation
import de.bixilon.moodle.api.requests.Request
import de.bixilon.moodle.api.serializer.JSONSerializer
import org.checkerframework.common.value.qual.IntRange

class CoreMessageGetConversationsRequest(
    private val userId: @IntRange(from = 0) Int,
    private val limitFrom: @IntRange(from = 0) Int = -1,
    private val limitNumber: @IntRange(from = 0) Int = -1,
    private val type: String? = null,
    private val onlyFavourites: Boolean? = null,
    private val mergeSelf: Boolean? = null,
) : Request {


    override fun write(data: MutableMap<String, Any>) {
        data["userid"] = userId
        if (limitFrom > 0) {
            data["limitfrom"] = limitFrom
        }
        if (limitNumber > 0) {
            data["limitnum"] = limitNumber
        }
        if (type != null) {
            data[type] = type
        }
        if (onlyFavourites != null) {
            data["favourites"] = onlyFavourites
        }
        if (mergeSelf != null) {
            data["mergeself"] = mergeSelf
        }
    }

    override fun readResponse(moodleAPI: AsyncMoodleAPI, data: Any): List<Conversation> {
        val conversations = mutableListOf<Conversation>()
        for (entry in (data as Map<String, Any>)["conversations"] as List<Map<*, *>>) {
            val conversation = JSONSerializer.CONVERSATION_ADAPTER.fromJsonValue(entry)!!
            conversation.moodleAPI = moodleAPI
            conversations.add(conversation)
        }
        return conversations.toList()
    }
}
