/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.requests.core.message

import de.bixilon.moodle.api.async.AsyncMoodleAPI
import de.bixilon.moodle.api.objects.Message
import de.bixilon.moodle.api.requests.Request
import de.bixilon.moodle.api.serializer.JSONSerializer
import org.checkerframework.common.value.qual.IntRange

class CoreMessageGetConversationMessagesRequest(
    private val currentUserId: @IntRange(from = 0) Int,
    private val conversationId: @IntRange(from = 0) Int,
    private val limitFrom: @IntRange(from = 0) Int = -1,
    private val limitNumber: @IntRange(from = 0) Int = -1,
    private val newestFirst: Boolean? = null,
    private val timeFrom: @IntRange(from = 0) Long = -1,
) : Request {


    override fun write(data: MutableMap<String, Any>) {
        data["currentuserid"] = currentUserId
        data["convid"] = conversationId
        if (limitFrom > 0) {
            data["limitfrom"] = limitFrom
        }
        if (limitNumber > 0) {
            data["limitnum"] = limitNumber
        }
        if (newestFirst != null) {
            data["newest"] = newestFirst
        }
        if (timeFrom > 0) {
            data["timefrom"] = timeFrom
        }
    }

    override fun readResponse(moodleAPI: AsyncMoodleAPI, data: Any): List<Message> {
        val messages = mutableListOf<Message>()
        for (entry in (data as Map<String, Any>)["messages"] as List<Map<*, *>>) {
            val message = JSONSerializer.MESSAGE_ADAPTER.fromJsonValue(entry)!!
            message.moodleAPI = moodleAPI
            messages.add(message)
        }
        return messages.toList()
    }
}
