/*
 * Moodle Kotlin API
 * Copyright (C) 2021 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with moodle.org, the original developer of moodle
 *
 */

package de.bixilon.moodle.api.requests.block.starredcourses

import de.bixilon.moodle.api.async.AsyncMoodleAPI
import de.bixilon.moodle.api.objects.Course
import de.bixilon.moodle.api.requests.Request
import de.bixilon.moodle.api.serializer.JSONSerializer
import org.checkerframework.common.value.qual.IntRange

class BlockStarredCoursesGetStarredCoursesRequest : Request {
    private val limit: Int

    constructor() {
        limit = -1
    }

    constructor(limit: @IntRange(from = 0) Int) {
        this.limit = limit
    }

    override fun write(data: MutableMap<String, Any>) {
        if (limit > 0) {
            data["limit"] = limit
        }
    }

    override fun readResponse(moodleAPI: AsyncMoodleAPI, data: Any): List<Course> {
        val courses = mutableListOf<Course>()
        for (entry in data as List<*>) {
            val course = JSONSerializer.COURSE_ADAPTER.fromJsonValue(entry as Map<*, *>)!!
            course.moodleAPI = moodleAPI
            courses.add(course)
        }
        return courses.toList()
    }
}
